from Bio import SeqIO
import argparse
import os

def extract(seq_file, site_list):
    prefix = os.path.basename(seq_file).split(".")[0]
    o_name = prefix+"_extract.csv"
    aln = SeqIO.to_dict(SeqIO.parse(seq_file, "fasta"))
    sites = {}
    agg_sites = []
    for k,v in aln.items():
        for site_num in site_list:
            site = v[site_num-1]
            sites.setdefault(k,[]).append(site)
    with open(o_name,'w') as outfile:
        outfile.write("Leaf_name")
        for i in site_list:
            outfile.write(",%s"%i)
        outfile.write("\n")
        for k,v in sites.items():
            k = k.replace("[","").replace("]","")
            outfile.write("%s"%k)
            for i in v:
                outfile.write(",%s"%i)
            outfile.write("\n")
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Extract a site and create a dataset for ancestral character state reconstruction")
    parser.add_argument('-a', metavar = 'Alignment', type = str, required = True,
                        help = "Path to alignment file")
    parser.add_argument('-s', metavar = 'Site Number', type=int,nargs='+', required = True,
                        help = "Site number in alignment (begin counting at 1)")
    parser.add_argument('-d', metavar = "drill", type = str,
                        help = "Extension for recursive input search")
    args = parser.parse_args()
    if args.d:
        files = [x for x in os.listdir(args.a) if os.path.splitext(x)[1] == args.d]
        for i in files:
            extract(os.path.normpath(os.path.join(args.a,i)),args.s)
    else:
        extract(args.a,args.s)
