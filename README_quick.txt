SeqSniffer v0.0.1
##BETA WARNING##

This script is still in development and has not been thoroughly tested outside of the windows OS.  Please double check all outputs and report
bugs to andrew@swafford.com.

=====README BEGIN======
This script will take either a single alignment or a directory full of alignments and extract the amino acids at user-specified sites
and place them into a CSV matrix. The user must then substitute numeric values for the alphabetical nucleotide or amino acids that were
extracted to use the matrix with corHMM or other ancestral state reconstruction programs.

Options:
-a	path to alignment or directory of alignments.
-s	site(s) Multiple sites should be separated by spaces
-d	initiate a recursive search, user must supply the extension of the files they wish to be discovered (i.e. "aln" discovers "file.aln").
	only use this option if -a indicates a directory.